/**
 * Mocking client-server processing
 */
import _flights from './flights.json'


const TIMEOUT = 100;

export default {
    getFlights: (cb, timeout) => setTimeout(() => cb(_flights.flights), timeout || TIMEOUT),
}
