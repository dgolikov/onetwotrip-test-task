import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import {getFacetValues} from "../reducers/facets";
import {changeCarrier} from "../actions/index";
import {getCarriers} from "../reducers/flights";
import Select from "../components/Select";

const Facets = ({facetValues, carriers, changeCarrier}) => (
    <div className="facet-wrapper">
        <div className="facet">
            <Select onChange={(code) => changeCarrier(code)} value={facetValues.carrier} options={carriers}/>
        </div>
    </div>
);

Facets.propTypes = {
    facetValues: PropTypes.shape({
        carrier: PropTypes.string.isRequired,
    }).isRequired,
    carriers: PropTypes.arrayOf(PropTypes.shape({
        code: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
    })).isRequired,
    changeCarrier: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    facetValues: getFacetValues(state),
    carriers: getCarriers(state)
});

export default connect(
    mapStateToProps,
    {changeCarrier}
)(Facets)
