import React from 'react'
import './app.css'
import Facets from "./Facets";
import Flights from "./Flights";


const App = () => (
    <div className="main">
        <div className="container">
            <div className="container__top-block">
                <Facets />
            </div>
            <div className="container__content-block">
                <Flights />
            </div>
        </div>
    </div>
);

export default App
