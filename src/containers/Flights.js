import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import {getFlights} from "../reducers/flights";
import Flight from "../components/Flight";


const Flights = ({flights}) => (
    <div className="flights-wrapper">
        {flights.map(flight =>
            <div className="flights-wrapper__element" key={flight.id}>
                <Flight flight={flight}/>
            </div>
        )}
    </div>
);

Flights.propTypes = {
    flights: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
    flights: getFlights(state),
});

export default connect(
    mapStateToProps,
)(Flights)
