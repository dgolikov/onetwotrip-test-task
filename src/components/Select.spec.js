import React from 'react'
import {shallow} from 'enzyme'
import Select from './Select'


const setup = ({options}) => {
    const actions = {
        onChange: jest.fn()
    };

    const component = shallow(
        <Select options={options} {...actions}/>
    );

    return {
        component,
        actions,
        options: component.find('option'),
    }
};

let optionsProp;

describe('Select component', () => {
    beforeEach(() => {
        optionsProp = [
            {
                code: 'all',
                name: 'Все авиакомпании'
            },
            {
                code: 'Aeroflot',
                name: 'Aeroflot'
            },
            {
                code: 'S7',
                name: 'S7'
            },
        ]
    });

    it('should render options', () => {
        const {options} = setup({options: optionsProp});
        expect(options.length).toEqual(3);
    });

    it('should render options', () => {
        const {actions, component} = setup({options: optionsProp});
        component.simulate('change', {target: { value : 'Aeroflot'}});
        expect(actions.onChange).toBeCalledWith('Aeroflot')
    });
});