import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import Flight from './Flight'


const setup = flight => {

    const component = shallow(
        <Flight flight={flight} />
    );

    return {
        component: component,
        timeDeparture: component.find('.flight__time-departure'),
        timeArrival: component.find('.flight__time-arrival'),
    }
};

let flightProps;

describe('Flight component', () => {
    beforeEach(() => {
        flightProps = {
            "id": 133,
            "direction": {
                "from": "Moscow",
                "to": "Samara"
            },
            "arrival": "2016-09-08T13:52:27.979Z",
            "departure": "2016-08-08T17:51:20.979Z",
            "carrier": "KLM"
        }
    });

    it('should render right time', () => {
        const { timeDeparture, timeArrival } = setup(flightProps);
        expect(timeDeparture.text()).toEqual(moment(flightProps.departure).format("HH:mm"));
        expect(timeArrival.text()).toEqual(moment(flightProps.arrival).format("HH:mm"));
    });
});