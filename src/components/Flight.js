import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import './flight.css'


const Flight = ({flight}) => (
    <div className="flight">
        <div className="flight__info clearfix">
            <div className="flight__info-left">
                <div className="flight__direction-from">
                    {flight.direction.from}
                </div>
                <div className="flight__time-departure">
                    {moment(flight.departure).format("HH:mm")}
                </div>
            </div>
            <div className="flight__info-right">
                <div className="flight__direction-to">
                    {flight.direction.to}
                </div>
                <div className="flight__time-arrival">
                    {moment(flight.arrival).format("HH:mm")}
                </div>
            </div>
        </div>
        <div className="flight__carrier">
            {flight.carrier}
        </div>
    </div>
);

Flight.propTypes = {
    flight: PropTypes.shape({
        id: PropTypes.number.isRequired,
        carrier: PropTypes.string.isRequired,
        direction: PropTypes.shape({
            from: PropTypes.string.isRequired,
            to: PropTypes.string.isRequired,
        }),
        arrival: PropTypes.string.isRequired,
        departure: PropTypes.string.isRequired,
    }).isRequired,
};

export default Flight
