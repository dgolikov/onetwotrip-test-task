import React from 'react'
import PropTypes from 'prop-types'


const Select = ({options, value, onChange}) => (
    <select onChange={(event) => onChange && onChange(event.target.value)} value={value}>
        {options.map(option =>
            <option value={option.code} key={option.code}>{option.name}</option>
        )}
    </select>
);

Select.propTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
        code: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
    })).isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func
};

export default Select
