import reducer, * as flights from './flights'
import * as ActionTypes from '../constants/ActionTypes'


describe('reducers', () => {
    describe('flights', () => {
        let state;

        describe('initial state', () => {
            beforeEach(() => {
                state = {};
                state.facets = reducer(undefined, {})
            });

            it('is empty arrays', () => {
                const flightsArray = flights.getFlights(state);
                expect(flightsArray).toEqual([]);
                const carriersArray = flights.getCarriers(state);
                expect(carriersArray).toEqual([]);

            });
        });

        describe('when flights are received', () => {

            beforeEach(() => {
                state = {};
                state.flights = reducer({}, {
                    type: ActionTypes.RECEIVE_FLIGHTS,
                    flights: [
                        {
                            "id": 123,
                            "direction": {
                                "from": "Moscow",
                                "to": "Berlin"
                            },
                            "arrival": "2016-06-08T19:52:27.979Z",
                            "departure": "2016-06-08T17:51:20.979Z",
                            "carrier": "S7"
                        },
                        {
                            "id": 193,
                            "direction": {
                                "from": "Moscow",
                                "to": "New York"
                            },
                            "arrival": "2016-06-08T21:52:27.979Z",
                            "departure": "2016-06-08T17:51:20.979Z",
                            "carrier": "S7"
                        },
                    ]
                }, {carrier: 'all'})
            });

            it('contains flights from the action', () => {
                const flightsArray = flights.getFlights(state);
                expect(flightsArray[0]).toEqual({
                    "id": 123,
                    "direction": {
                        "from": "Moscow",
                        "to": "Berlin"
                    },
                    "arrival": "2016-06-08T19:52:27.979Z",
                    "departure": "2016-06-08T17:51:20.979Z",
                    "carrier": "S7"
                });
                expect(flightsArray[1]).toEqual({
                    "id": 193,
                    "direction": {
                        "from": "Moscow",
                        "to": "New York"
                    },
                    "arrival": "2016-06-08T21:52:27.979Z",
                    "departure": "2016-06-08T17:51:20.979Z",
                    "carrier": "S7"
                })
            });

            it('contains no other flights', () => {
                const flightsArray = flights.getFlights(state);
                expect(flightsArray[2]).toEqual(undefined)
            });

            it('contains unique carriers from flights plus ALL element', () => {
                const carriersArray = flights.getCarriers(state);
                expect(carriersArray).toEqual([
                    {
                        code: "all",
                        name: "Все авиакомпании",
                    },
                    {
                        code: "S7",
                        name: "S7",
                    },
                ])
            });
        })
    })
});