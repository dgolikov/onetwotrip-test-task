import reducer, * as facets from './facets'
import * as ActionTypes from '../constants/ActionTypes'


describe('reducers', () => {
    describe('facets', () => {
        let state;

        describe('initial state', () => {
            beforeEach(() => {
                state = {};
                state.facets = reducer(undefined, {})
            });

            it('contains initial carrier facet', () => {
                const facetValues = facets.getFacetValues(state);
                expect(facetValues.carrier).toEqual('all');
            });
        });

        describe('when carrier are changed', () => {
            beforeEach(() => {
                state = {};
                state.facets = reducer({}, {
                    type: ActionTypes.CHANGE_CARRIER,
                    carrier: 'S7'
                })
            });

            it('contains carrier facet from the action', () => {
                const facetValues = facets.getFacetValues(state);
                expect(facetValues.carrier).toEqual('S7');
            });
        });
    });
});