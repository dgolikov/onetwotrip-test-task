import {CHANGE_CARRIER} from '../constants/ActionTypes'


export const CARRIER_ALL = 'all';

const facets = (state, action) => {
    switch (action.type) {
        case CHANGE_CARRIER:
            return {
                ...state,
                carrier: action.carrier,
            };
        default:
            return state || {
                    carrier: CARRIER_ALL // по умолчанию фасет "Авиакомпания" будет иметь значение указанное здесь.
                }
    }
};

export default facets;

export const getFacetValues = (state) => {
    return state.facets || {};
};