import {RECEIVE_FLIGHTS, CHANGE_CARRIER} from '../constants/ActionTypes'
import {CARRIER_ALL} from "./facets";


const filterByFacets = (flights, facets) => {
    return flights.filter((_) => {
        return facets.carrier === CARRIER_ALL || facets.carrier === _.carrier
    })
};

const filteredList = (state = [], action, facets) => {
    if (!facets) {
        return state || {
                original: [],
                filtered: [],
            };
    }
    switch (action.type) {
        case RECEIVE_FLIGHTS:
            return {
                original: action.flights,
                filtered: filterByFacets(action.flights, facets), // необходимо рассчитывать фильтрацию и при первичном получении, т.к. существуют значения фасетов заданные по умолчанию
            };
        case CHANGE_CARRIER:
            return {
                ...state,
                filtered: filterByFacets(state.original, facets), // при изменении фасета происходит перерасчет фильтрации
            };
        default:
            return state || {
                    original: [],
                    filtered: [],
                }
    }
};

export default filteredList

export const getCarriers = (state) => {
    if (state.flights && state.flights.original) {
        const carriers = state.flights.original.map((_) => {
            return _.carrier
        }).filter((v, i, a) => a.indexOf(v) === i).map((_)=>{
            return {code: _, name: _}
        }); // map создает массив из авиакомпаний => filter оставляет только уникальные элементы => map создает массив объектов(code, name) авиакомпаний
        carriers.unshift({code: CARRIER_ALL, name: "Все авиакомпании"}); // добавляется в начало всегда существующий элемент для выбора - "Все авиакомпании"
        return carriers;
    }
    return [];
};

export const getFlights = (state) => {
    return (state.flights && state.flights.filtered) || [];
};


