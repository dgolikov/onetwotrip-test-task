import {combineReducers} from 'redux'
import facets from './facets'
import flights from './flights'
import {RECEIVE_FLIGHTS, CHANGE_CARRIER} from "../constants/ActionTypes";


const combinedReducers = combineReducers({
    facets,
    flights
});

function crossSliceReducer(state, action) { // необходим т.к. flights зависит от facets
    switch (action.type) {
        case CHANGE_CARRIER:
        case RECEIVE_FLIGHTS:
            return {
                ...state,
                flights: flights(state.flights, action, state.facets),
            };
        default:
            return state;
    }
}

const rootReducer = (state, action) => {
    const intermediateState = combinedReducers(state, action);
    return crossSliceReducer(intermediateState, action);
};

export default rootReducer;