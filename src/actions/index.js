import flights from '../api/flights'
import * as types from '../constants/ActionTypes'


const receiveFlights = flights => ({
    type: types.RECEIVE_FLIGHTS,
    flights
});


export const getFlights = () => dispatch => {
    flights.getFlights(flights => {
        dispatch(receiveFlights(flights))
    })
};

export const changeCarrier = (carrier) => (dispatch) => {
    dispatch({
        type: types.CHANGE_CARRIER,
        carrier,
    })
};
